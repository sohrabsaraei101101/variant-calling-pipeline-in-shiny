# Variant calling pipeline in Shiny

author: Sohrab Saraei (http://saraei.me/)

This is a variant calling pipeline that is implemented in R Shiny. What makes this workflow unique is that with short configuration and few click, quality control report of the BAM files, VCF files and summary statistics of the VCF files is generated. This pipeline is also packaged into a docker container (credit to sevenbridges group for making Shiny and Rstudio server packed docker container for building and debuggin of this pipeline possible URL: https://bioconductor.org/packages/release/bioc/vignettes/sevenbridges/inst/doc/rstudio.html), which can translate to cross-platform minimum configuration pipeline. This pipeline is based on Causey et. al. 2018 (10.1038/s41598-018-25022-6) paper. There are few modifications applied to the present pipeline which is the parallelism in variant calling by splitting the input BAM files by chromosome and run GATK MuTect2 on the smaller chunks, which can significantly reduce the run-time of the pipeline. This pipeline is multi-platform if it is ran through docker image. However, the barebone shiny app can only be ran through distributions of Linux.

Pipeline schematic: https://www.nature.com/articles/s41598-018-25022-6/figures/1

# Dependencies

Below the list of dependencies and instructions for instulation can be found.

*  samtools

sudo apt-get install samtools
*  bamtools

sudo apt-get install bamtools
*  gatk

https://software.broadinstitute.org/gatk/documentation/quickstart.php
*  qualimap

http://qualimap.bioinfo.cipf.es/

**note**: Qualimap uses X11 window system for generating the visualization.

*  Kintr [R package]
*  vcfR [R package]
*  ape [R package]
*  shiny [R package]
*  shinyjs [R package]
*  Rmarkdown [R package]


*  Docker: By using docker, you would not need to worry about any of the previous dependencies, as they are taken care of in the docker image. Please for installing docker for your platform visit the link below:

[Click on get started]
https://www.docker.com/

# The docker image 

https://drive.google.com/open?id=1ZvqBvRp9zwfuTl6QcZJcRjV3aa-puNky

# Video tutorials

## Standalone shiny app on local host

In this tutorial you can see the functionality of the pipeline.

https://youtu.be/13P1UKRMGw0

# Pipeline using the docker 
In this tutorial I elaborated on how the docker works.

https://youtu.be/seqJJ-5uEDA

### The following commands are useful for running the docker
docker load -i [point to the downloaded image]

docker images

docker run -d -p 8787:8787 -p 3838:3838 -v /tmp/.X11-unix:/tmp/.X11-unix -v [address to a directory containing data and References]:/ext/ -e DISPLAY=$DISPLAY --name rstudio_shiny_server [the name or id of the image]

docker container ls

docker exec -it [running container] /bin/bash

**Note for Windows users**: 


*  Valume sharing (meaning -v dir:dir part of docker run command) is not as straight forwad and Unix-based operating systems. You can find some useful information in articles like this: https://rominirani.com/docker-on-windows-mounting-host-directories-d96f3f056a2c
*  For X11 port forwarding (in docker run command as -v /tmp/.X11-unix:/tmp/.X11-unix and -e DISPLAY=$DISPLAY) you should look into X11 and docker. They are discussed in posts like (as of 20th of March 2019): https://robscode.onl/docker-run-gui-app-in-linux-container-on-windows-host/ and https://stackoverflow.com/questions/40024892/windows-10-docker-host-display-gui-application-from-linux-container


## Slurm and the pipeline - a development idea

https://youtu.be/CNReWW0aSgM

# To do
*  Checking the inputs of the user and send them proper messages if the the necessary input, for sucessfull execution of the pipeline is not met
*  Implementing and testing scatter-gather paralellism using workload managers like Slurm and etc.
*  A cancel button that can stop the pipeline at anytime